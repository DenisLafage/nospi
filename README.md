# NoSpi2 project

This repository contains the R code and genetic dataset used to develop the NoSpi2 
primer. 

This primer was designed to:
- not bind to spiders of the oval calamistrum clade (Lycosidae and closely 
phylogenetically related spipder families)
- amplify most of arthropod families

# Related publication:
- Preprint on [peerJ](https://peerj.com/preprints/27854/)
- Submitted to Methods in Ecology and Evolution
- Authors: Denis Lafage, Vasco Elbrecht, Jordan Cuff, Dirk Steinke, 
Peter Hambäck and Ann Erlandsson