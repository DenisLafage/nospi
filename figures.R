# Libraries

library(tidyverse)
library(ggplot2)
library(fmsb)
library(extrafont)
font_import()
loadfonts(device="postscript")       #Register fonts for Windows bitmap output
  

# Prepare data

data_prey_bias <- prey_bias %>% 
  gather(key = "primer", value = "bias", 2:5)

data_prey_bias$primer <- str_replace_all(data_prey_bias$primer,"_prey", "")
data_prey_bias$primer <- str_replace_all(data_prey_bias$primer,"Br2", "BR2_F")
data_prey_bias$primer <- str_replace_all(data_prey_bias$primer,"HCO", "HCO_R")
data_prey_bias$primer <- str_replace_all(data_prey_bias$primer,"LCO", "LCO_F")

# Bar plot

p_bias_prey <- ggplot(data_prey_bias, aes(preys,bias))
p_bias_prey + geom_col(aes(fill = primer),
                       position = "dodge2") + coord_flip() + theme_bw()

                       

# Radar charts by prey group and primer type

par(family = "Fira Sans",mfrow = c(2,2))

## Aquatic preys, Forward primers

aquat_bias_F <- data_prey_bias %>% 
  filter(primer %in% c("NoSpi2F_prey", "LCO_prey")) %>% 
  filter(preys %in% c( "Ephemeroptera","Megaloptera", "Odonata",
                      "Plecoptera", "Trichoptera", "Coleoptera", "Collembola", "Diptera")) %>% 
  spread(key = preys, value = bias)
aquat_bias_F <- as.data.frame(aquat_bias_F[2:9])
rownames(aquat_bias_F)= c("LCO", "NoSpi2")

# add extrem values
aquat_bias_F=rbind(rep(160,8) , rep(0,8) , aquat_bias_F)


# Plot 2: Same plot with custom features
colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart( aquat_bias_F  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,160,40), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(aquat_bias_F[-c(1,2),]), bty = "n", pch=20 , col=colors_in , text.col = "black", cex=1.2, pt.cex=3)


## Terrestrial preys, Forward primers

terr_bias_F <- data_prey_bias %>% 
  filter(primer %in% c("NoSpi2F_prey", "LCO_prey")) %>% 
  filter(preys %in% c("Archaeognatha", "Blattodea", "Dermaptera", "Embioptera","Grylloblattodea", "Hemiptera",
                      "Hymenoptera", "Isoptera", "Lepidoptera", "Mantodea", "Mantophasmatodea","Mecoptera", "Neuroptera","Orthoptera",
                      "Phasmatodea", "Psocodea", "Raphidioptera", "Siphonaptera", "Strepsiptera", "Thysanoptera",
                      "Zoraptera","Zygentoma")) %>% 
  spread(key = preys, value = bias)
terr_bias_F <- as.data.frame(terr_bias_F[2:23])
rownames(terr_bias_F)= c("LCO", "NoSpi2")

# add extrem values
terr_bias_F=rbind(rep(700,22) , rep(0,22) , terr_bias_F)


# Plot 2: Same plot with custom features
colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart(terr_bias_F  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,700,175), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(terr_bias_F[-c(1,2),]), bty = "n", pch=20 , col=colors_in , text.col = "black", cex=1.2, pt.cex=3)


# Radar charts by prey group and primer type

## Aquatic preys, reverse primers

aquat_bias_R <- data_prey_bias %>% 
  filter(primer %in% c("Br2_prey", "HCO_prey")) %>% 
  filter(preys %in% c("Ephemeroptera","Megaloptera", "Odonata",
                      "Plecoptera", "Trichoptera", "Coleoptera", "Collembola", "Diptera")) %>% 
  spread(key = preys, value = bias) %>% 
  arrange(desc(primer))
aquat_bias_R <- as.data.frame(aquat_bias_R[2:9])
rownames(aquat_bias_R)= c("HCO", "BR2")

# add extrem values
aquat_bias_R=rbind(rep(140,8) , rep(0,8) , aquat_bias_R)

# Plot 2: Same plot with custom features
colors_border_R=c( rgb(0.7,0.5,0.1,0.9), rgb(0.1,0.2,0.5,0.9))
colors_in_R=c(rgb(0.7,0.5,0.5,0.4), rgb(0.1,0.2,0.5,0.4))
radarchart( aquat_bias_R  , axistype=1 , 
            #custom polygon
            pcol=colors_border_R , pfcol=colors_in_R , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,140,35), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(aquat_bias_R[-c(1,2),]), bty = "n", pch=20 , col=colors_in_R , text.col = "black", cex=1.2, pt.cex=3)


## Terrestrial preys, reverse primers

terr_bias_R <- data_prey_bias %>% 
  filter(primer %in% c("Br2_prey", "HCO_prey")) %>% 
  filter(preys %in% c("Archaeognatha", "Blattodea", "Dermaptera", "Embioptera","Grylloblattodea", "Hemiptera",
                      "Hymenoptera", "Isoptera", "Lepidoptera", "Mantodea", "Mantophasmatodea","Mecoptera", "Neuroptera","Orthoptera",
                      "Phasmatodea", "Psocodea", "Raphidioptera", "Siphonaptera", "Strepsiptera", "Thysanoptera",
                      "Zoraptera","Zygentoma")) %>% 
  spread(key = preys, value = bias)%>% 
  arrange(desc(primer))
terr_bias_R <- as.data.frame(terr_bias_R[2:23])
rownames(terr_bias_R)= c("HCO", "BR2")

# add extrem values
terr_bias_R=rbind(rep(240,22) , rep(0,22) , terr_bias_R)


# Plot 2: Same plot with custom features
radarchart(terr_bias_R  , axistype=1 , 
           #custom polygon
           pcol=colors_border_R , pfcol=colors_in_R , plwd=4 , plty=1,
           #custom the grid
           cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,240,60), cglwd=0.8,
           #custom labels
           vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(terr_bias_R[-c(1,2),]), bty = "n", pch=20 , col=colors_in , text.col = "black", cex=1.2, pt.cex=3)


# Field test, barchart with prey per species

g_barcode <- seq_tab_final %>% 
  mutate(Order = as.character(Order)) %>% 
  mutate(Order = case_when(Order %in% c("Entomobryomorpha", "Oribatida","Symphypleona", 
                                        "Acari", "Collembola") ~ "Acari / Collembola",
                           Order %in% "Stylommatophora"~ "Gasteropoda",
                           TRUE ~ Order)) %>% 
  filter(!Order %in% c("Cypriniformes")) %>% 
  group_by(Order, species) %>% 
  summarise(n_detect = n())

p1 <- ggplot(data = g_barcode, aes(species, n_detect, fill = Order))
p1 + geom_bar(position = "fill",stat="identity") +
  scale_fill_brewer(palette="Set3") + theme_bw() +
  xlab("Species") + ylab("Proportion of prey detected") +
  geom_text(aes(label = n_detect), position = "fill") + guides(fill=guide_legend(title="Class or Order"))